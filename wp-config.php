<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'yardnine');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'e(#!isr?I<cO#zHcrQr+|#4ZD?e3(V3bD!+cul5uH7h+|qsZrY_>twH _UcAJm|0');
define('SECURE_AUTH_KEY',  'w3jgyto0r%W|b<kNs&=Srer*#W0o3a{E-HT(HZldK2}gZ_2;kxdlMXa}au?Jv5If');
define('LOGGED_IN_KEY',    '+G`_$+;)XIqagb`$*0_+|&}W{EL;zSNqqs:iJ GG#v^O&/s[fkmb:g+ZxzY]_8hb');
define('NONCE_KEY',        'UC~$D+38*X+A`9+]/A4r0b5ZR}+W^#{3><z0XqNPo@^*/p1F-|Pgm-a]-oG+QYk#');
define('AUTH_SALT',        '|iP+o<LYAC%=>%r6O_EG{(Ia;2Je*pYwpULcIh1J-E3`_Kh8& 5.j@!k!}#cKDKn');
define('SECURE_AUTH_SALT', 'OB3b+~0C~5a?.XG`6e+O^jf11wS,41-<:eaE|t^SyCTxrY^0~:}-Uv^u/Bw{C8~J');
define('LOGGED_IN_SALT',   'UVY7ajO^1%Jk+6(ztXeK*SkB0[=Xo#D=+}b/P-qdYxwVl&!;^}%[j^c*,Y|z+@u[');
define('NONCE_SALT',       'pJEF-x-/gN<D%GcVPtj&L/UU2:#O;baA3l|se/xO<Yh-VqQScB[A3#*5vTW?I`Qd');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'xyz_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

<?php
/**
 * The footer for theme.
 *
 * @package    WordPress
 * @subpackage themeName
 * @since      themeName 1.0
 */

?>
<?php
$f_content = get_field( 'f_content', 'options' );


?>
<footer class="page-main">

	<div class="container">
      
       <div class="footer__items-wrapper">
          
           <div class="footer__text-wrapper"><?php
               
            if ($f_content):
            echo $f_content;
            endif;?>
               
           </div><!--/.footer__text-wrapper-->
           
           <!-- <div class="footer__social-wrapper"><?php
               
            /*if(have_rows( 'social_links', 'options' )):
                while(have_rows( 'social_links', 'options' )): the_row();
            
                $s_icon = get_sub_field( 'social_icon' );
                $s_link = get_sub_field( 'social_link' );
                ?>
                <a href="<?php $s_link['url'];?>" <?php if($s_link['target']):?> target="<?php echo $s_link['target'];?>" <?php endif;?>><img src="<?php echo $s_icon;?>"></a><?php
                endwhile;
            endif;*/?>
            
            </div> --> <!--/.footer__social-wrapper-->
           
       </div><!--/.footer__items-wrapper-->
        
        <div class="sub-footer"><?php
            if(have_rows( 'footer_items', 'options' )):
                while(have_rows( 'footer_items', 'options')): the_row();
                
                $set_as_link = get_sub_field( 'set_item_as_link' );
               
                if ($set_as_link):
                $link = get_sub_field( 'link' );?>
                <a href="<?php echo $link['url'];?>" <?php if($link['target']):?> target="<?php echo $link['target'];?>"<?php endif;?>><?php echo $link['title'];?></a><?php
                else :
                $text = get_sub_field( 'text' );?>
                <span><?php echo $text;?></span><?php
                endif;
            
                endwhile;
            endif;?>
            
        </div><!--/.sub-footer-->
    
	</div><!--/.container-->
	
</footer>

</div> <!-- /#page -->

<?php wp_footer(); ?>

</body>
</html>

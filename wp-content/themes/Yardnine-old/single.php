<?php
/**
 * The single post page template.
 *
 * @package    WordPress
 * @subpackage themeName
 * @since      themeName 1.0
 */

get_header();
the_post();

$location_title = get_field( 'location_title' );
$extra_info = get_field( 'extra_info' );
$post_img = get_the_post_thumbnail_url();
?>

    <section class="hero__section">
        <div class="hero__image" style="background-image:url(<?php echo $post_img;?>);"></div>
        
        <div class="hero__container">
           <div class="container">
               <?php if($location_title):?>
                    <span class="location-title">
                        <?php echo $location_title;?>
                    </span>
                <?php endif;?>
                <h1><?php the_title(); ?></h1>
                
                <?php if($extra_info):?>
                    <span class="extra-info">
                        <?php echo $extra_info;?>
                    </span>
                <?php endif;?>
            </div><!--/.container-->

            <?php if( have_rows('section') ): ?>
                <?php $sectionCount = count( get_field('section') ); ?>
                <?php if ($sectionCount > 1) : ?>
                    <div class="project-inner-nav">
                        <div class="container">
                            <ul>
                                <?php while ( have_rows('section') ) : the_row(); ?>
                                    <?php if( get_row_layout() == 'office' ): ?>
                                        <li><a href="#office-tab">Office</a></li>
                                    <?php elseif( get_row_layout() == 'retail' ): ?>
                                        <li><a href="#retail-tab">Retail</a></li>
                                    <?php elseif( get_row_layout() == 'residential' || get_row_layout() == 'residential_sidebar' ): ?>
                                        <li><a href="#residential-tab">Residential</a></li>
                                    <?php endif; ?>
                                <?php endwhile; ?>
                            </ul>
                        </div>
                    </div>
                <?php endif; ?>
            <?php endif; ?>
        </div><!--/.hero__container-->
    </section><!--/.hero__section-->
    
	<main class="b-accordion__main-wrapper">
        <?php /*if(have_rows( 'accordion' )):
            $count = 1;
            while(have_rows( 'accordion' )):the_row();
        
                get_theme_part('sections/single-project/full-page-accordion',array(
                'count' => $count
                ));
            $count ++;
            endwhile;
        endif;*/ ?>
        
        <?php if( have_rows('section') ):
            while ( have_rows('section') ) : the_row(); ?>

                <?php if( get_row_layout() == 'office' ): ?>
                    <div class="b-accordion__item office-accordion" id="office-tab">
                        <div class="b-accordion__content">
                           <div class="inner">
                                <div class="simple-content__wrapper">
                                    <div class="container">
                                        <h4 class="section-title">Office</h4>
                                        <div class="row project-intro-wrapper">   
                                            <div class="change-font-size">
                                                <?php the_sub_field('main_description'); ?>
                                            </div>
                                            <div class="single-sidebar">
                                                <?php the_sub_field('sidebar_content'); ?>
                                            </div>
                                        </div>

                                        <?php if( have_rows('office_sections') ):
                                            while ( have_rows('office_sections') ) : the_row(); ?>

                                                <?php if( get_row_layout() == 'photos' ): ?>
                                                    <div class="row office-image-wrapper <?php the_sub_field('left_image_width'); ?>">
                                                        <!-- <div class="single-office-image office-left-image" style="background-image: url(<?php #the_sub_field('left_image'); ?>);"></div> -->
                                                        <!-- left image -->
                                                        <?php 
                                                        $images = get_sub_field('left_image');
                                                        if( $images ): ?>
                                                            <div class="project-slider__main-wrapper single-office-image office-left-image">
                                                                <section class="single-project-slider__wrapper">
                                                                    <?php foreach( $images as $image ): ?>
                                                                        <div class="project-slider__item"> 
                                                                            <div class="project-slider__image" style="background-image:url(<?php echo $image['url']; ?>);" ></div>
                                                                        </div>
                                                                    <?php endforeach; ?>
                                                                </section><!--/.project-slider__wrapper-->
                                                            </div>
                                                        <?php endif; ?>
                                                        <!-- right image -->
                                                        <?php 
                                                        $images = get_sub_field('right_gallery');
                                                        if( $images ): ?>
                                                            <div class="project-slider__main-wrapper single-office-image office-right-image">
                                                                <section class="single-project-slider__wrapper">
                                                                    <?php foreach( $images as $image ): ?>
                                                                        <div class="project-slider__item"> 
                                                                            <div class="project-slider__image" style="background-image:url(<?php echo $image['url']; ?>);" ></div>
                                                                        </div>
                                                                    <?php endforeach; ?>
                                                                </section><!--/.project-slider__wrapper-->
                                                            </div>
                                                        <?php endif; ?>
                                                    </div>
                                                <?php elseif( get_row_layout() == 'carousel' ): ?>
                                            
                                                    <?php if (have_rows('carousel')):?>
                                                        <div class="project-slider__main-wrapper">
                                                            <section class="single-project-slider__wrapper">
                                                                <?php while(have_rows('carousel')): the_row(); ?>

                                                                    <div class="project-slider__item"> 
                                                                        <div class="project-slider__image" style="background-image:url(<?php the_sub_field('slide'); ?>);" ></div>
                                                                    </div>
                                                                <?php endwhile; ?>
                                                            </section><!--/.project-slider__wrapper-->
                                                        </div>
                                                    <?php endif;?>

                                                <?php endif;?>

                                            <?php endwhile;
                                        endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                <?php elseif( get_row_layout() == 'retail' ): ?>

                    <div class="b-accordion__item retail-accordion" id="retail-tab">
                        <div class="b-accordion__content">
                           <div class="inner">
                                <div class="simple-content__wrapper">
                                    <div class="container">
                                        <div class="row retail-content office-image-wrapper">
                                            <div class="office-left-image">
                                                <h4 class="section-title">Retail</h4>
                                                <div class="change-font-size">
                                                    <?php the_sub_field('main_description'); ?>
                                                </div>
                                            </div>
                                            <div class="single-office-image office-right-image"> 
                                                <img src="<?php the_sub_field('image'); ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                <?php elseif( get_row_layout() == 'residential' ): ?>

                    <div class="b-accordion__item retail-accordion" id="residential-tab">
                        <div class="b-accordion__content">
                           <div class="inner">
                                <div class="simple-content__wrapper">
                                    <div class="container">
                                        <div class="row residential-content office-image-wrapper">
                                            <div class="single-office-image office-right-image">
                                                <img src="<?php the_sub_field('top_image'); ?>">
                                            </div>
                                            <div class="office-left-image">
                                                <h4 class="section-title">Residential</h4>
                                                <div class="change-font-size">
                                                    <?php the_sub_field('main_description'); ?>
                                                </div>
                                            </div>
                                        </div>
                                        <?php if (get_sub_field('bottom_image')) : ?>
                                            <div class="row residential-content residential-bottom">
                                                <div class="single-office-image">
                                                    <img src="<?php the_sub_field('bottom_image'); ?>">
                                                </div>
                                            </div>
                                        <?php endif; ?>

                                        <?php if (have_rows('carousel')):?>
                                            <div class="row residential-content residential-bottom">
                                                <div class="project-slider__main-wrapper">
                                                    <section class="single-project-slider__wrapper">
                                                        <?php while(have_rows('carousel')): the_row(); ?>

                                                            <div class="project-slider__item"> 
                                                                <div class="project-slider__image" style="background-image:url(<?php the_sub_field('slide'); ?>);" ></div>
                                                            </div>
                                                        <?php endwhile; ?>
                                                    </section><!--/.project-slider__wrapper-->
                                                </div>
                                            </div>
                                        <?php endif;?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                <?php elseif( get_row_layout() == 'residential_sidebar' ): ?>
                    <div class="b-accordion__item office-accordion" id="office-tab">
                        <div class="b-accordion__content">
                           <div class="inner">
                                <div class="simple-content__wrapper">
                                    <div class="container">
                                        <h4 class="section-title">Residential</h4>
                                        <div class="row project-intro-wrapper">   
                                            <div class="change-font-size">
                                                <?php the_sub_field('main_description'); ?>
                                            </div>
                                            <div class="single-sidebar">
                                                <?php the_sub_field('sidebar_content'); ?>
                                            </div>
                                        </div>

                                        <?php if (have_rows('carousel')):?>
                                            <div class="project-slider__main-wrapper">
                                                <section class="single-project-slider__wrapper">
                                                    <?php while(have_rows('carousel')): the_row(); ?>

                                                        <div class="project-slider__item"> 
                                                            <div class="project-slider__image" style="background-image:url(<?php the_sub_field('slide'); ?>);" ></div>
                                                        </div>
                                                    <?php endwhile; ?>
                                                </section><!--/.project-slider__wrapper-->
                                            </div>
                                        <?php endif;?>

                                        <?php if( have_rows('residential_imageries') ):
                                            while ( have_rows('residential_imageries') ) : the_row(); ?>

                                                <?php if( get_row_layout() == 'photos' ): ?>
                                                    <div class="row office-image-wrapper <?php the_sub_field('left_image_width'); ?>">
                                                        <!-- <div class="single-office-image office-left-image" style="background-image: url(<?php #the_sub_field('left_image'); ?>);"></div> -->
                                                        <!-- left image -->
                                                        <?php 
                                                        $images = get_sub_field('left_image');
                                                        if( $images ): ?>
                                                            <div class="project-slider__main-wrapper single-office-image office-left-image">
                                                                <section class="single-project-slider__wrapper">
                                                                    <?php foreach( $images as $image ): ?>
                                                                        <div class="project-slider__item"> 
                                                                            <div class="project-slider__image" style="background-image:url(<?php echo $image['url']; ?>);" ></div>
                                                                        </div>
                                                                    <?php endforeach; ?>
                                                                </section><!--/.project-slider__wrapper-->
                                                            </div>
                                                        <?php endif; ?>
                                                        <!-- right image -->
                                                        <?php 
                                                        $images = get_sub_field('right_gallery');
                                                        if( $images ): ?>
                                                            <div class="project-slider__main-wrapper single-office-image office-right-image">
                                                                <section class="single-project-slider__wrapper">
                                                                    <?php foreach( $images as $image ): ?>
                                                                        <div class="project-slider__item"> 
                                                                            <div class="project-slider__image" style="background-image:url(<?php echo $image['url']; ?>);" ></div>
                                                                        </div>
                                                                    <?php endforeach; ?>
                                                                </section><!--/.project-slider__wrapper-->
                                                            </div>
                                                        <?php endif; ?>
                                                    </div>
                                                <?php elseif( get_row_layout() == 'carousel' ): ?>
                                            
                                                    <?php if (have_rows('carousel')):?>
                                                        <div class="project-slider__main-wrapper">
                                                            <section class="single-project-slider__wrapper">
                                                                <?php while(have_rows('carousel')): the_row(); ?>

                                                                    <div class="project-slider__item"> 
                                                                        <div class="project-slider__image" style="background-image:url(<?php the_sub_field('slide'); ?>);" ></div>
                                                                    </div>
                                                                <?php endwhile; ?>
                                                            </section><!--/.project-slider__wrapper-->
                                                        </div>
                                                    <?php endif;?>

                                                <?php endif;?>

                                            <?php endwhile;
                                        endif; ?>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                <?php endif;

            endwhile;
        endif; ?>
      
	</main> <!--/.b-accordion__main-wrapper-->

    <?php get_theme_part('sections/single-project/next-and-previous');?>
   
<?php

get_footer();

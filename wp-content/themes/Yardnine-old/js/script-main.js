function getHashFilter() {
  var hash = location.hash;
  // get filter=filterName
  var matches = location.hash.match( /filter=([^&]+)/i );
  var hashFilter = matches && matches[1];
  return hashFilter && decodeURIComponent( hashFilter );
}

! function(e) {
    function t(i) {
        if (n[i]) return n[i].exports;
        var o = n[i] = {
            i: i,
            l: !1,
            exports: {}
        };
        return e[i].call(o.exports, o, o.exports, t), o.l = !0, o.exports
    }
    var n = {};
    t.m = e, t.c = n, t.d = function(e, n, i) {
        t.o(e, n) || Object.defineProperty(e, n, {
            configurable: !1,
            enumerable: !0,
            get: i
        })
    }, t.n = function(e) {
        var n = e && e.__esModule ? function() {
            return e.default
        } : function() {
            return e
        };
        return t.d(n, "a", n), n
    }, t.o = function(e, t) {
        return Object.prototype.hasOwnProperty.call(e, t)
    }, t.p = "", t(t.s = 0)
}([function(e, t, n) {
    e.exports = n(1)
}, function(e, t, n) {
    "use strict";
    var i = n(2),
        o = function(e) {
            return e && e.__esModule ? e : {
                default: e
            }
        }(i);
    o.default.init(), window.onload = function() {
        o.default.loaded()
    }, window.onresize = function() {
        o.default.resized()
    }, jQuery(window).on("scroll", function() {
        o.default.scrolled()
    })
}, function(e, t, n) {
    "use strict";

    function i(e) {
        return e && e.__esModule ? e : {
            default: e
        }
    }

    function o(e, t) {
        if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function")
    }
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    var r = function() {
            function e(e, t) {
                for (var n = 0; n < t.length; n++) {
                    var i = t[n];
                    i.enumerable = i.enumerable || !1, i.configurable = !0, "value" in i && (i.writable = !0), Object.defineProperty(e, i.key, i)
                }
            }
            return function(t, n, i) {
                return n && e(t.prototype, n), i && e(t, i), t
            }
        }(),
        a = n(3),
        s = i(a),
        l = n(4),
        c = i(l),
        u = n(6),
        d = i(u),
        f = n(7),
        v = i(f),
        h = n(8),
        p = i(h),
        m = n(9),
        g = n(11),
        y = i(g),
        b = n(12),
        _ = i(b),
        w = n(13),
        T = i(w),
        C = n(14),
        j = n(16),
        k = i(j),
        x = n(17),
        O = i(x),
        P = n(18),
        M = i(P),
        S = n(19),
        A = i(S),
        L = n(20),
        Q = i(L),
        W = n(21),
        I = i(W),
        z = n(22),
        D = function() {
            function e() {
                o(this, e)
            }
            return r(e, [{
                key: "init",
                value: function() {
                    document.querySelector("html").classList.remove("no-js"), (0, s.default)(), c.default.init(), (0, v.default)(), (0, d.default)(), (0, p.default)(), m.sliderAbout.init(), m.sliderProject.init(), m.sliderCarousel.carousel(), (0, y.default)(), (0, T.default)(), C.servAccordion.init(), C.aboutAccordion.init(), (0, k.default)(), (0, A.default)(), (0, Q.default)(), (0, I.default)(), (0, z.initScroll)(), (0, z.smoothToNextSection)(), (0, M.default)()
                }
            }, {
                key: "loaded",
                value: function() {
                    document.querySelector("body").classList.add("page-has-loaded"), C.fullAccordion.fullPageInit()
                }
            }, {
                key: "resized",
                value: function() {
                    c.default.init()
                }
            }, {
                key: "scrolled",
                value: function() {
                    (0, _.default)(), (0, O.default)()
                }
            }]), e
        }(),
        H = new D;
    t.default = H
}, function(e, t, n) {
    "use strict";

    function i() {
        var e = a(".btn-hamburger"),
            t = a("header nav"),
            n = a("body");
        e.on("click", function(i) {
            i.preventDefault(), e.hasClass("is-open") ? (e.removeClass("is-open"), n.removeClass("lock"), t.slideUp(), r()) : (e.addClass("is-open"), a(".fixed").length > 0 && n.addClass("lock"), t.slideDown(), o())
        })
    }

    function o() {
        a(".bg-transparent").length > 0 && a(window).scrollTop() < 1 && (a("header").hasClass("change-bg-style") || a("header").addClass("change-bg-style"))
    }

    function r() {
        a(".bg-transparent").length > 0 && a(window).scrollTop() < 1 && a("header").hasClass("change-bg-style") && a("header").removeClass("change-bg-style")
    }
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    var a = jQuery.noConflict();
    t.default = i
}, function(e, t, n) {
    "use strict";
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    var i = n(5),
        o = function(e) {
            return e && e.__esModule ? e : {
                default: e
            }
        }(i),
        r = {
            gridWrapper: ".projects__wrapper",
            gridItem: ".projects__item"
        },
        a = new o.default(r);
    t.default = a
}, function(e, t, n) {
    "use strict";

    function i(e, t) {
        if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function")
    }
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    var o = function() {
            function e(e, t) {
                for (var n = 0; n < t.length; n++) {
                    var i = t[n];
                    i.enumerable = i.enumerable || !1, i.configurable = !0, "value" in i && (i.writable = !0), Object.defineProperty(e, i.key, i)
                }
            }
            return function(t, n, i) {
                return n && e(t.prototype, n), i && e(t, i), t
            }
        }(),
        r = jQuery.noConflict(),
        a = window.matchMedia("screen and (max-width:1024px)"),
        s = window.matchMedia("screen and (max-width:767px)"),
        l = function() {
            function e(t) {
                var n = t.gridWrapper,
                    o = t.gridItem;
                i(this, e), this.gridWrapper = n, this.gridItem = o
            }
            return o(e, [{
                key: "isotopeGrid",
                value: function() {
                    s.matches ? r(this.gridWrapper).isotope({
                        itemSelector: this.gridItem,
                        percentPosition: !0,
                        masonry: {
                            columnWidth: ".grid-sizer",
                            gutter: 20
                        }
                    }) : (a.matches, r(this.gridWrapper).isotope({
                        itemSelector: this.gridItem,
                        percentPosition: !0,
                        masonry: {
                            columnWidth: ".grid-sizer",
                            gutter: 38
                        }
                    }))
                }
            }, {
                key: "init",
                value: function() {
                    this.isotopeGrid()
                }
            }]), e
        }();
    t.default = l
}, function(e, t, n) {
    "use strict";

    function i(e) {
        var t = e.find(".marker"),
            n = {
                zoom: 22,
                center: new google.maps.LatLng(0, 0),
                disableDefaultUI: !0,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                styles: [{
                    featureType: "all",
                    elementType: "geometry.fill",
                    stylers: [{
                        weight: "2.00"
                    }]
                }, {
                    featureType: "all",
                    elementType: "geometry.stroke",
                    stylers: [{
                        color: "#9c9c9c"
                    }]
                }, {
                    featureType: "all",
                    elementType: "labels.text",
                    stylers: [{
                        visibility: "on"
                    }]
                }, {
                    featureType: "landscape",
                    elementType: "all",
                    stylers: [{
                        color: "#f2f2f2"
                    }]
                }, {
                    featureType: "landscape",
                    elementType: "geometry.fill",
                    stylers: [{
                        color: "#ffffff"
                    }]
                }, {
                    featureType: "landscape.man_made",
                    elementType: "geometry.fill",
                    stylers: [{
                        color: "#ffffff"
                    }]
                }, {
                    featureType: "poi",
                    elementType: "all",
                    stylers: [{
                        visibility: "off"
                    }]
                }, {
                    featureType: "road",
                    elementType: "all",
                    stylers: [{
                        saturation: -100
                    }, {
                        lightness: 45
                    }]
                }, {
                    featureType: "road",
                    elementType: "geometry.fill",
                    stylers: [{
                        color: "#eeeeee"
                    }]
                }, {
                    featureType: "road",
                    elementType: "labels.text.fill",
                    stylers: [{
                        color: "#7b7b7b"
                    }]
                }, {
                    featureType: "road",
                    elementType: "labels.text.stroke",
                    stylers: [{
                        color: "#ffffff"
                    }]
                }, {
                    featureType: "road.highway",
                    elementType: "all",
                    stylers: [{
                        visibility: "simplified"
                    }]
                }, {
                    featureType: "road.arterial",
                    elementType: "labels.icon",
                    stylers: [{
                        visibility: "off"
                    }]
                }, {
                    featureType: "transit",
                    elementType: "all",
                    stylers: [{
                        visibility: "off"
                    }]
                }, {
                    featureType: "water",
                    elementType: "all",
                    stylers: [{
                        color: "#46bcec"
                    }, {
                        visibility: "on"
                    }]
                }, {
                    featureType: "water",
                    elementType: "geometry.fill",
                    stylers: [{
                        color: "#c8d7d4"
                    }]
                }, {
                    featureType: "water",
                    elementType: "labels.text.fill",
                    stylers: [{
                        color: "#070707"
                    }]
                }, {
                    featureType: "water",
                    elementType: "labels.text.stroke",
                    stylers: [{
                        color: "#ffffff"
                    }]
                }]
            },
            i = new google.maps.Map(e[0], n);
        return i.markers = [], t.each(function() {
            o(s(this), i)
        }), r(i), i
    }

    function o(e, t) {
        var n = new google.maps.LatLng(e.attr("data-lat"), e.attr("data-lng")),
            i = {
                url: WP.templateUrl + "/images/marker.svg",
                scaledSize: new google.maps.Size(185, 63)
            },
            o = new google.maps.Marker({
                position: n,
                map: t,
                optimized: !1,
                icon: i
            });
        if (t.markers.push(o), e.html()) {
            var r = new google.maps.InfoWindow({
                content: e.html()
            });
            google.maps.event.addListener(o, "click", function() {
                r.open(t, o)
            })
        }
    }

    function r(e) {
        var t = new google.maps.LatLngBounds;
        s.each(e.markers, function(e, n) {
            var i = new google.maps.LatLng(n.position.lat(), n.position.lng());
            t.extend(i)
        }), 1 == e.markers.length ? (e.setCenter(t.getCenter()), e.setZoom(16)) : e.fitBounds(t)
    }

    function a() {
        s(document).ready(function() {
            s(".acf-map").each(function() {
                i(s(this)).panBy(-10, -38)
            })
        })
    }
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    var s = jQuery.noConflict();
    t.default = a
}, function(e, t, n) {
    "use strict";

    function i() {
        var e = d.offset().top,
            t = e + d.outerHeight(),
            n = l(window).scrollTop(),
            i = n + l(window).height();
        return t > n && e < i
    }

    function o() {
        d.length > 0 && (setTimeout(function() {
            /*c.css("height", "0"), i() && u.addClass("noscroll")*/
        }, 1500), c.one("transitionend", function() {
            u.removeClass("noscroll"), c.remove()
        }))
    }

    function r() {
        if (d.length > 0) {
            var e = c.offset().top,
                t = v.offset().top,
                n = v.outerHeight(),
                i = h.outerHeight();
            if (e >= t) {
                var o = n / i,
                    r = 3 * o;
                f.css({
                    transition: "height " + r + "s linear"
                }), a(), s()
            }
        }
    }

    function a() {
        f.css({
            height: "100%"
        })
    }

    function s() {
        clearInterval(p)
    }
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    var l = jQuery.noConflict(),
        c = l(".mask"),
        u = l("body"),
        d = l(".intro-section"),
        f = l(".show-image"),
        v = l(".intro-section__logo"),
        h = l(".intro-section__image"),
        p = setInterval(function() {
            r()
        }, 1);
    t.default = o
}, function(e, t, n) {
    "use strict";

    function i() {
        if (document.querySelector("main.content")) {
            var e = function() {
                    r = i.offsetHeight, n.style.paddingTop = r + "px"
                },
                t = function() {
                    if (!document.querySelector(".home")) {
                        window.pageYOffset >= a ? (i.classList.add("fixed"), o && o.classList.add("visible")) : (i.classList.remove("fixed"), o.classList.remove("visible")), i.classList.contains("fixed") ? e() : n.style.paddingTop = 0, 0 == window.pageYOffset && (i.classList.remove("fixed"), n.style.paddingTop = 0)
                    }
                },
                n = document.querySelector("main.content"),
                i = document.querySelector("header.page-main"),
                o = document.querySelector(".logo__wrapper.change-state"),
                r = i.offsetHeight,
                a = i.offsetTop;
            i.classList.contains("fixed") && window.addEventListener("resize", e), window.addEventListener("scroll", t)
        }
    }
    Object.defineProperty(t, "__esModule", {
        value: !0
    }), t.default = i
}, function(e, t, n) {
    "use strict";
    Object.defineProperty(t, "__esModule", {
        value: !0
    }), t.sliderCarousel = t.sliderProject = t.sliderAbout = void 0;
    var i = n(10),
        o = function(e) {
            return e && e.__esModule ? e : {
                default: e
            }
        }(i),
        r = jQuery.noConflict(),
        a = {
            section: r(".about__wrapper"),
            slidesToShow: 2,
            slidesToScroll: 1,
            varWidth: !0
        },
        s = {
            section: ".project-slider__wrapper",
            slidesToShow: 1,
            SlidesToScroll: 1,
            varWidth: !1
        },
        l = {
            section: ".carousel__wrapper",
            slidesToShow: 2,
            SlidesToScroll: 1,
            varWidth: !1
        },
        c = new o.default(a),
        u = new o.default(s),
        d = new o.default(l);
    t.sliderAbout = c, t.sliderProject = u, t.sliderCarousel = d
}, function(e, t, n) {
    "use strict";

    function i(e, t) {
        if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function")
    }
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    var o = function() {
            function e(e, t) {
                for (var n = 0; n < t.length; n++) {
                    var i = t[n];
                    i.enumerable = i.enumerable || !1, i.configurable = !0, "value" in i && (i.writable = !0), Object.defineProperty(e, i.key, i)
                }
            }
            return function(t, n, i) {
                return n && e(t.prototype, n), i && e(t, i), t
            }
        }(),
        r = jQuery.noConflict(),
        a = function() {
            function e(t) {
                var n = t.section,
                    o = t.slidesToShow,
                    r = t.slidesToScroll,
                    a = t.varWidth,
                    s = void 0 !== a && a;
                i(this, e), this.section = n, this.slidesToShow = o, this.slidesToScroll = r, this.varWidth = s
            }
            return o(e, [{
                key: "init",
                value: function() {
                    r(this.section).slick({
                        infinite: !0,
                        slidesToShow: this.slidesToShow,
                        slidesToScroll: this.slidesToScroll,
                        variableWidth: this.varWidth,
                        autoplay: true,
                        autoplaySpeed: 5000
                    })
                }
            }, {
                key: "carousel",
                value: function() {
                    r(this.section).slick({
                        infinite: !0,
                        slide: ".car-link",
                        slidesToShow: this.slidesToShow,
                        slidesToScroll: this.slidesToScroll,
                        variableWidth: this.varWidth,
                        autoplay: true,
                        autoplaySpeed: 5000,
                        appendArrows: r(".controls-wrapper"),
                        nextArrow: ".next-link",
                        prevArrow: ".prev-link"
                    })
                }
            }]), e
        }();
    t.default = a
}, function(e, t, n) {
    "use strict";

    function i() {
        var e = a(this).attr("data-filter");
        l.isotope({
            filter: e
        })
    }

    function o() {
        var e = a(".filter-button");
        a("body").find(e).removeClass("active-btn active-filter"), a(this).addClass("active-btn active-filter");
        var filterAttr = a( this ).attr('data-filter');
        // set filter in hash
        location.hash = 'filter=' + encodeURIComponent( filterAttr );
    }

    function r() {
        s.on("click", "button", i), s.on("click", "button", o)
    }
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    var a = jQuery.noConflict(),
        s = a(".filter__wrapper"),
        l = a(".projects__wrapper");
    t.default = r
}, function(e, t, n) {
    "use strict";

    function i() {
        var e = o(window).scrollTop();
        o(".hero__image").css({
            transform: "translate3d(0px," + e / 20 + "% , 0)"
        })
    }
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    var o = jQuery.noConflict();
    t.default = i
}, function(e, t, n) {
    "use strict";

    function i() {
        function e(e) {
            e.preventDefault(), i.fadeOut();
            var t = o("#" + o(this).attr("href"));
            o(this).hasClass("active") ? /*t.fadeOut()*/ t.fadeIn() : t.fadeIn()
        }
        var t = o(".tab__item"),
            n = o(".tab__item.active"),
            i = o(".tab__img");
        ! function(e) {
            o("#" + e.attr("href")).fadeIn()
        }(n), t.on("click", e)
    }
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    var o = jQuery.noConflict();
    t.default = i
}, function(e, t, n) {
    "use strict";
    Object.defineProperty(t, "__esModule", {
        value: !0
    }), t.aboutAccordion = t.servAccordion = t.fullAccordion = void 0;
    var i = n(15),
        o = function(e) {
            return e && e.__esModule ? e : {
                default: e
            }
        }(i),
        r = {
            accTrigger: ".b-accordion__trigger",
            accContent: ".b-accordion__content",
            activeContent: ".b-accordion__content.active",
            icon: ".b-accordion__plus",
            activeTrigger: ".b-accordion__trigger.active"
        },
        a = {
            accTrigger: ".tab__item",
            accContent: ".tab__content",
            activeContent: ".tab__content.active",
            icon: ".plus",
            activeTrigger: ".tab__item.active"
        },
        s = {
            accTrigger: ".member-item__name",
            accContent: ".member-item__description",
            activeContent: ".member-item__description.active",
            icon: ".plus-member",
            activeTrigger: ".member-item__name.active"
        },
        l = new o.default(r),
        c = new o.default(a),
        u = new o.default(s, !1);
    t.fullAccordion = l, t.servAccordion = c, t.aboutAccordion = u
}, function(e, t, n) {
    "use strict";

    function i(e, t) {
        if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function")
    }
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    var o = function() {
            function e(e, t) {
                for (var n = 0; n < t.length; n++) {
                    var i = t[n];
                    i.enumerable = i.enumerable || !1, i.configurable = !0, "value" in i && (i.writable = !0), Object.defineProperty(e, i.key, i)
                }
            }
            return function(t, n, i) {
                return n && e(t.prototype, n), i && e(t, i), t
            }
        }(),
        r = jQuery.noConflict(),
        a = function() {
            function e(t) {
                var n = t.accTrigger,
                    o = t.accContent,
                    r = t.activeContent,
                    a = t.icon,
                    s = t.activeTrigger,
                    l = !(arguments.length > 1 && void 0 !== arguments[1]) || arguments[1];
                i(this, e), this.accTrigger = n, this.accContent = o, this.activeContent = r, this.icon = a, this.activeTrigger = s, this.closeOther = l
            }
            return o(e, [{
                key: "accordion",
                value: function(e) {
                    e.preventDefault();
                    var t = r(e.currentTarget);
                    t.hasClass("active") ? t.addClass("active") /*this.removeActive(e)*/ : (this.removeActive(e), t.addClass("active"), t.find(r(this.icon)).addClass("open"), t.next().addClass("active").slideDown())
                }
            }, {
                key: "fullPageAccordion",
                value: function(e) {
                    e.preventDefault();
                    var t = r(e.currentTarget),
                        n = t.next().find(".inner"),
                        i = n.height();
                    t.hasClass("active") ? this.removeFullAccordion(e) : (this.removeFullAccordion(e), t.addClass("active"), t.find(r(this.icon)).addClass("open"), t.next().addClass("active").css("height", i))
                }
            }, {
                key: "removeFullAccordion",
                value: function(e) {
                    var t = r(e.currentTarget),
                        n = t.next().find(".inner"),
                        i = n.height();
                    this.closeOther ? (r(this.accTrigger).removeClass("active"), r(this.accContent).removeClass("active").css("height", 0), r(this.icon).removeClass("open")) : (t.removeClass("active"), t.find(r(this.icon)).removeClass("open"), t.next().addClass("active").css("height", i))
                }
            }, {
                key: "removeActive",
                value: function(e) {
                    var t = r(e.currentTarget);
                    this.closeOther ? (r(this.accTrigger).removeClass("active"), r(this.accContent).removeClass("active").slideUp(), r(this.icon).removeClass("open")) : (t.removeClass("active"), t.find(r(this.icon)).removeClass("open"), t.next().addClass("active").slideUp())
                }
            }, {
                key: "fullPageInit",
                value: function() {
                    var e = this,
                        t = r(this.activeContent).find(".inner"),
                        n = t.height();
                    r(this.activeContent).css("height", n), r(this.activeTrigger).find(r(this.icon)).addClass("open"), r(this.accTrigger).on("click", function(t) {
                        return e.fullPageAccordion(t)
                    })
                }
            }, {
                key: "init",
                value: function() {
                    var e = this;
                    r(this.activeContent).slideDown(), r(this.activeTrigger).find(r(this.icon)).addClass("open"), r(this.accTrigger).on("click", function(t) {
                        return e.accordion(t)
                    })
                }
            }]), e
        }();
    t.default = a
}, function(e, t, n) {
    "use strict";

    function i() {
        function e() {
            o(this).closest(".member-item").find(".member-item__name").click()
        }
        o(".member-item__image").on("click", e)
    }
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    var o = jQuery.noConflict();
    t.default = i
}, function(e, t, n) {
    "use strict";

    function i() {
        o(".bg-transparent").length > 0 && (o(window).scrollTop() > 1 ? r.addClass("change-bg-style") : r.removeClass("change-bg-style")), o(".global-header").length > 0 && (o(window).scrollTop() > 1 ? o(".global-header").addClass("change-bg-style") : o(".global-header").removeClass("change-bg-style"))
    }
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    var o = jQuery.noConflict(),
        r = o(".page-main.bg-transparent");
    t.default = i
}, function(e, t, n) {
    "use strict";

    function i() {
        function e() {
            t.removeClass("blue"), o(this).addClass("blue")
        }
        var t = o(".current-menu-item");
        t.first().addClass("blue"), t.on("click", e)
    }
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    var o = jQuery.noConflict();
    t.default = i
}, function(e, t, n) {
    "use strict";

    function i() {
        if (l.length > 0) {
            var e = a.outerHeight(!0),
                t = s.outerHeight(),
                n = t + e,
                i = "calc(100vh - " + n + "px )";
            r.css("min-height", i)
        }
    }
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    var o = jQuery.noConflict(),
        r = o("main"),
        a = o("footer"),
        s = o("header"),
        l = o(".error404");
    t.default = i
}, function(e, t, n) {
    "use strict";

    function i() {
        r.each(function() {
            var e = o(this);
            !e.parent().hasClass("video-container") && a.matches && e.wrap('<div class="video-container"></div>')
        })
    }
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    var o = jQuery.noConflict(),
        r = o('iframe[src*="youtube"], iframe[src*="vimeo"]'),
        a = window.matchMedia("screen and (max-width: 767px)");
    t.default = i
}, function(e, t, n) {
    "use strict";

    function i() {
        l.matches && (a(".visible-btn").length > 0 ? (s.children().removeClass("visible-btn"), s.find("button:not(.active-btn)").hide()) : (s.find("button:not(.active-btn)").show(), s.children().addClass("visible-btn"), a(this).removeClass("visible-btn").addClass("active-btn")))
    }

    function o() {
        l.matches && (s.children().removeClass("active-btn"), a(this).prependTo(".change-to-dropdown").addClass("active-btn"))
    }

    function r() {
        s.on("click", "button", i), s.on("click", "button", o)
    }
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    var a = jQuery.noConflict(),
        s = a(".change-to-dropdown"),
        l = window.matchMedia("screen and (max-width: 767px)");
    t.default = r
}, function(e, t, n) {
    "use strict";

    function i() {
        !l(".home").length > 0 && c.removeAttr("data-page")
    }

    function o(e) {
        if (c.attr("data-page")) {
            var t = l(this).attr("href") ? l(this).attr("href") : l(this).find("a").attr("href");
            if (e.preventDefault(), -1 !== t.indexOf(f)) {
                var n = t.substr(t.indexOf("#") + 1, t.length),
                    i = "[data-scroll = '" + n + "']",
                    o = l(i);
                if (o.length) {
                    var r = o.offset().top - u;
                    l("html,body").animate({
                        scrollTop: r
                    }, 1e3)
                }
            }
        }
    }

    function r() {
        var e = l("header.main").outerHeight(),
            t = window.location.href;
        if (-1 !== t.indexOf("#")) {
            /*var n = "[data-scroll=" + t.substr(t.indexOf("#") + 1, t.length) + "]";
            if (n.length) {
                var i = l(n).offset().top - e;
                l("html,body").animate({
                    scrollTop: i
                }, 1e3)
            }*/
        }
    }

    function a() {
        i(), c.on("click", ".scroll-to-page", o), r()
    }

    function s() {
        l(".intro-section").on("click", function() {
            l("html, body").animate({
                scrollTop: l("header").offset().top
            }, 800)
            /*l(this).slideUp(800);*/
        })
    }
    Object.defineProperty(t, "__esModule", {
        value: !0
    });
    var l = jQuery.noConflict(),
        c = l("body"),
        u = l("header.main").outerHeight(),
        d = window.location.href,
        f = d.substr(0, d.indexOf("#"));
    t.initScroll = a, t.smoothToNextSection = s
}]);

(function($){
    $('.btn__show-more').click(function(e){
        e.preventDefault();
        $(this).hide();
        $('.text__show-more').slideDown();
    });

    // scroll to anchor
    $('a[href*="#"]:not([href="#"])').click(function() {
        var target = $(this.hash);
        if (target.length) {
            $('html, body').animate({
                scrollTop: target.offset().top - 70
            }, 1000);
            return false;
        }
    });

    window.wasScrolled = false;
    var nohome = sessionStorage.getItem('nohome')
    if ($('.home').length && nohome != 'true') {
        $('body').fadeTo(300,1);
        $(window).bind('scroll',function(){
            if (!window.wasScrolled){
                $('html, body').animate({scrollTop:$('header').position().top},800, function(){
                    $('.intro-section').remove();
                    $('html, body').animate({scrollTop: 0 },0);
                    $('.logo__wrapper').css({opacity: 1});
                    $('.page-main, main.content').addClass('fixed');
                });
            }
            window.wasScrolled = true;
        })
    }

    if (nohome == 'true') {
        $('body').fadeTo(300,1);
        $('.intro-section').remove();
        $('html, body').animate({scrollTop: 0 },0);
        $('.page-main').addClass('fixed');
        $('.logo__wrapper').css({opacity: 1});
    }

    if ($('.single').length || $('.page-template-tpl-projects').length) {
        sessionStorage.setItem('nohome', 'true');
    }

    function onHashchange() {
        var hashFilter = getHashFilter();
        if ( !hashFilter || hashFilter == null ) {
          return;
        }
        $('.projects__wrapper').isotope({
          filter: hashFilter
        });
        if ( hashFilter ) {
            $(".filter-button").removeClass("active-btn active-filter");
            $('.filter-button[data-filter="' + hashFilter + '"]').addClass('active-btn active-filter');
        }
    }
    onHashchange();


    // Active menu class
    var topMenu = jQuery("#menu-main-menu"),
        offset = 45,
        topMenuHeight = topMenu.outerHeight()+offset,
        // All list items
        menuItems =  topMenu.find('a[href*="#"]'),
        // Anchors corresponding to menu items
        scrollItems = menuItems.map(function(){
          var href = jQuery(this).attr("href"),
          id = href.substring(href.indexOf('#')),
          item = jQuery(id);
          //console.log(item)
          if (item.length) { return item; }
        });

    // Bind to scroll
    jQuery(window).scroll(function(){
       // Get container scroll position
       var fromTop = jQuery(this).scrollTop()+topMenuHeight;

       // Get id of current scroll item
       var cur = scrollItems.map(function(){
         if (jQuery(this).offset().top < fromTop)
           return this;
       });

       // Get the id of the current element
       cur = cur[cur.length-1];
       var id = cur && cur.length ? cur[0].id : "";               

       menuItems.parent().removeClass("active");
       if(id){
            menuItems.parent().end().filter("[href*='#"+id+"']").parent().addClass("active");
       }

    })

    jQuery('.partnership_logos__wrapper').slick({
        slidesToShow: 3,
        slidesToScroll: 3,
        autoplay: true,
        autoplaySpeed: 5000
    });

    jQuery('.single-project-slider__wrapper').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 5000,
        fade: true
    });

})(jQuery);
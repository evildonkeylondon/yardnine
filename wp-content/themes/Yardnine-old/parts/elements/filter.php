

    <div class="filter__wrapper change-to-dropdown">
       <span class="dropdown-icon"></span>
            
            <button class="filter-button" data-filter="*">ALL</button>
       <?php
        if (have_rows( 'p_filter' )):
        $count = 1;
            while (have_rows( 'p_filter' )) : the_row();
       
                $filter_name = get_sub_field( 'filter_name' );
                $filter_category = get_sub_field( 'f_category' );
            
                 $f_slug = $filter_category->slug; ?>
        
                <button class="filter-button<?php if($count == 1):?> active-btn<?php endif;?>" data-filter=".<?php echo $f_slug;?>"> <?php echo $filter_name;?></button>
        
                <?php $count ++;
            endwhile;
        endif;?>
        
    </div> <!--/.filter-wrapper-->
<?php
$member = !empty($member) ? $member : false;

$img = get_the_post_thumbnail_url($member->ID, 'team_img');
$title = $member->post_title;
?>
   
    <div class="member-item">
        
        <div class="member-item__image">
            <img src="<?php echo $img;?>">
            
        </div> <!--/.member-item__image-->
        
        <div class="member-item__info">
            <div class="member-item__name">
                <h4>
                    <?php 
                    echo $title;?>
                    <span class="plus plus-member"></span>
                </h4>
            </div>
            <div class="member-item__description">
                <?php
                if ($member->post_content):
                echo $member->post_content;
                endif;
            ?>
            </div>
           
        </div><!--/.member-item__info-->
        
    </div><!--/.member-item-->
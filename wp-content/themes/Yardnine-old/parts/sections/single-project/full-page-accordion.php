<?php
$count;
$a_title = get_sub_field( 'accordion_title' );
$a_content = get_sub_field( 'accordion_content');
    
?>
    
    <div class="b-accordion__item">
		    
		    <div class="b-accordion__content <?php if ($count == 1):?> active<?php endif;?>">
               <div class="inner">
                <div class="container">
                    <?php     
                        if($a_content):
                        echo $a_content;
                    endif;?>
                </div>
                
                 <?php if (have_rows('accordion_slider')):?>
                    <div class="project-slider__main-wrapper">
                        <section class="project-slider__wrapper"><?php

                            while(have_rows('accordion_slider')):the_row();

                                  $img_field = get_sub_field('accordion_slider_image'); 
                                  $img = wp_get_attachment_image_src($img_field, 'project-slider');?>

                                  <div class="project-slider__item"> 
                                      <div class="project-slider__image" style="background-image:url(<?php echo $img[0];?>);" ></div>
                                  
                                  </div><?php
                            endwhile;?>
                       

                        </section><!--/.project-slider__wrapper-->
                    </div>
                <?php endif;?>
		       </div>
		    </div> <!--/.b-accordion__content-->
		         
    </div><!--/.b-accordion__item-->
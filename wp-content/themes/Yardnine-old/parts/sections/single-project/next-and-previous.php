<?php 
$next_post = get_next_post();
$prev_post = get_previous_post();
$link = get_field('p_link');
?>
    <section class="posts__wrapper">
        
        <section class="carousel__main-wrapper">
           
            <div class="carousel__container">
            
                <div class="carousel__wrapper">

                    <?php
                    global $post;
                    $postcat = get_the_category( $post->ID ); ?>

                    <?php 
                        $project_categories = [];
                        foreach ($postcat as $cat) {
                            $project_categories[] = $cat->term_id;
                        }
                        if(count($project_categories) == 1) {
                            $filter = current($project_categories);
                        } else {
                            $filter = 5;
                        }
                    ?> 

                    <?php $args = array(
                        'post_type' => 'post',
                        'cat' => $filter,
                        'orderby' => 'rand',
                        'post__not_in' => array( $post->ID )
                    );

                $post_query = new WP_Query($args);
                
                    if($post_query->have_posts() ) { ?>
                        <div class="controls-wrapper">
                            <span>Related projects</span>
                            <!-- <a href="<?php #echo $link['url'];?>" <?php #if($link['target']):?> target="<?php #echo $link['target'];?>"<?php #endif;?> ><img class="posts__img" src="<?php #echo get_template_directory_uri() . '/images/posts-icon.svg'; ?>"></a> -->
                            <a class="next-link" ><?php _e( '>', 'Yardnine' ); ?></a>
                            <a class="prev-link" ><?php _e( '<', 'Yardnine' ); ?></a>
                        </div>
                        <?php while($post_query->have_posts() ) {
                            $post_query->the_post();
                            $project_location_title = get_field( 'location_title', $post_query->ID); ?>
                    
                            <a class="car-link" href="<?php echo get_permalink(); ?>" style="background-image:url(<?php echo get_the_post_thumbnail_url();?>);">
                                <div class="projects__item-info">
                                    <div class="item-info-text-wrapper"><?php 
                                    if($project_location_title):?>
                                        <span class="location-item"><?php echo $project_location_title;?></span><?php
                                    endif;?>
                                    <span class="title-span"><?php the_title();?></span>
                                    </div>
                                </div>
                            </a>
                        <?php } ?>
                        <a class="related-all-link projects__link" href="/projects">
                            <span>See all projects <span class="plus"></span></span>
                        </a>
                    <?php } ?>
                    
               </div><!--/.carousel__wrapper-->
               
            </div> <!--/.container-->
            
        </section> <!--/.carousel__main-wrapper-->
        
    </section> <!--/.posts__wrapper-->
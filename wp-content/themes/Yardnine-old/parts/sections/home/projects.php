<?php
$class = !empty($class) ? $class : false;
$s_title = get_field( 'p_section_title' );
$link = get_field( 'h_link' );
$filter = !empty($filter) ? true : false;

?>
    <section class="projects__main-wrapper <?php if($class): echo $class; endif;?>">
        <div class="container"><?php
            if($s_title):?>
            <div class="projects__tittle section__title"> 
                <?php echo $s_title;?>
            </div><?php endif;
            
            if ($filter):
                get_theme_part('elements/filter');
            endif;
            
            if(have_rows( 'project_items' )):?>
            
            <div class="projects__wrapper">
               <div class="grid-sizer"></div><?php
                while(have_rows( 'project_items' )): the_row();
                
                $project = get_sub_field( 'project' );
                $project_title = $project[0]->post_title;
                $project_cat = get_the_category($project[0]->ID);
                $project_link = get_permalink($project[0]->ID);
                $project_location_title = get_field( 'location_title', $project[0]->ID);
                
                $width = get_sub_field( 'project_width' );
                $mobile_height = get_sub_field( 'project_height_on_mobile' );
                $remove_on_mobile = get_sub_field( 'remove_on_mobile' );
                $remove = 'remove-item';
                $img = get_the_post_thumbnail_url($project[0]->ID);
                $project_slug = array();
                
                if ($width == 'first'):
                $width_class = 'full-width';
                elseif ($width == 'second'):
                $width_class = 'medium-width';
                else:
                $width_class = 'small-width';
                endif;
                
                if ($mobile_height == 'first'):
                $mobile_class = 'full-height';
                elseif ($mobile_height == 'second'):
                $mobile_class = 'medium-height';
                else:
                $mobile_class = 'small-height';
                endif;
                
                
                
                foreach ($project_cat as $p=>$i)
                  
                array_push($project_slug, $project_cat[$p]->slug);
                   
                $p_slug = implode(' ',$project_slug);
                 
            if($project_link):?>
          
            <a href="<?php echo $project_link;?>" class="projects__item <?php echo $width_class; echo ' '.$mobile_class; if($remove_on_mobile): echo ' '. $remove; endif;?> <?php echo $p_slug;?>" style="background-image:url(<?php echo $img;?> );?>"><?php
            endif;?>
                
                <div class="projects__item-info">
                    <div class="item-info-text-wrapper"><?php 
                    if($project_location_title):?>
                        <span class="location-item"><?php echo $project_location_title;?></span><?php
                    endif;?>
                    <span><?php echo $project_title;?></span>
                    </div>
                </div>
                
            </a><!--/.projects__item-->
           
               
                <?php endwhile;?>
            </div><!--/.projects__wrapper-->
            <?php endif;?>
            
        </div> <!--/.container-->
       
    </section><!--/.projects__main-wrapper-->
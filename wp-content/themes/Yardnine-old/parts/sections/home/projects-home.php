<?php
$class = !empty($class) ? $class : false;
$s_title = get_field( 'p_section_title' );
$link = get_field( 'h_link' );
$filter = !empty($filter) ? true : false;

?>
    <section class="projects__main-wrapper <?php if($class): echo $class; endif;?>" id="projects">
        <div class="container"><?php
            if($s_title):?>
            <div class="projects__tittle section__title"> 
                <?php echo $s_title;?>
            </div><?php endif;
            
            if ($filter):
            get_theme_part('elements/filter');
            endif;
            
            if(have_rows( 'project_items' )):?>
            
            <div class="projects__wrapper">
               <div class="grid-sizer"></div><?php
                while(have_rows( 'project_items' )): the_row();

                $field = get_field_object('field_5af18a6a9d874');
                $value = get_sub_field('link');
                $label = $field['choices'][ $value ];

                $project_image = get_sub_field( 'project_image' );
                $project_title = $label;
                $project_link = get_sub_field( 'link' );
                
                $width = get_sub_field( 'project_width' );
                $mobile_height = get_sub_field( 'project_height_on_mobile' );
                $remove_on_mobile = get_sub_field( 'remove_on_mobile' );
                $remove = 'remove-item';
                
                if ($width == 'first'):
                $width_class = 'full-width';
                elseif ($width == 'second'):
                $width_class = 'medium-width';
                else:
                $width_class = 'small-width';
                endif;
                
                if ($mobile_height == 'first'):
                $mobile_class = 'full-height';
                elseif ($mobile_height == 'second'):
                $mobile_class = 'medium-height';
                else:
                $mobile_class = 'small-height';
                endif;
                 
            if($project_link):?>
          
            <a href="/projects/#filter=.<?php echo $project_link;?>" class="projects__item <?php echo $width_class; echo ' '.$mobile_class; if($remove_on_mobile): echo ' '. $remove; endif;?>" style="background-image:url(<?php echo $project_image;?> );?>"><?php
            endif;?>
                
                <div class="projects__item-info">
                    <div class="item-info-text-wrapper">
                    <span><?php echo $project_title;?></span>
                    </div>
                </div>
                
            </a><!--/.projects__item-->
           
               
                <?php endwhile;?>
            </div><!--/.projects__wrapper-->
            <?php endif;?><?php
            
            if ($link):?>
            <a href="<?php echo $link['url'];?>" class="projects__link"  <?php if ($link['target']):?> target="<?php echo $link['target'];?>" <?php endif;?>><?php echo $link['title'];?>
            <span class="plus"></span></a><?php
            endif;?>
            
        </div> <!--/.container-->
       
    </section><!--/.projects__main-wrapper-->
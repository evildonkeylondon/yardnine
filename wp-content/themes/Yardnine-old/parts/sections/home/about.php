<?php
$section_title = get_field( 't_section_title' );
$section_content = get_field( 'section_content' );
$team = get_field( 'team_members' );
?>

    <section class="about__main-wrapper" data-scroll="about" id="about">
        <div class="container">    
            <div class="about__content-wrapper">   
                <?php
                if($section_title):?>
                <div class="about__title section__title"><?php 
                    
                    echo $section_title;?>
                    
                </div><?php
                
                endif;
                
                if ($section_content):?>
                <div class="about__text-wrapper"><?php 
                    
                echo $section_content;?>
                
                </div> <!--/.about__text-wrapper--><?php
                
                endif;
                
                if($team):?>
                
                <div class="about__wrapper"><?php
                    
                    foreach ($team as $member) :
                    
                    get_theme_part('elements/team_member', array(
                    'member' => $member
                    ));
                    
                    endforeach;
                    ?>
                    
                </div><!--/.about__wrapper--><?php
                endif;?>
            </div>
        </div>
    </section> <!--/.about__main-wrapper-->
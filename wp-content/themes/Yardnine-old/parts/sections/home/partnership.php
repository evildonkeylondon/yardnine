<?php
$section_title = get_field( 'part_section_title' );
$section_content = get_field( 'part_section_content' );
$section_image = get_field( 'part_image' );
$logos = get_field( 'logos' );
?>

    <section class="about__main-wrapper" data-scroll="partnership" id="partnership">
        <div class="container">    
            <div class="partnership__content-wrapper">   
                <?php
                if($section_title):?>
                <div class="partnership__title section__title"><?php 
                    
                    echo $section_title;?>
                    
                </div><?php
                
                endif; ?>
                
                <div class="partnership__image">
                    <img src="<?php echo $section_image; ?>">
                </div>

                <?php if ($section_content):?>
                <div class="partnership__text-wrapper"><?php 
                    
                echo $section_content;?>

                    <?php if ($logos):?>
                    
                    <div class="partnership_logos__wrapper"><?php
                        
                        foreach ($logos as $logo) : ?>
                            
                            <div class="logo-slide">
                                <?php echo wp_get_attachment_image( $logo['ID'], $size ); ?>
                            </div>
                        
                        <?php #get_theme_part('elements/team_member', array(
                        #'member' => $member
                        #));
                        
                        endforeach;
                        ?>
                        
                    </div><!--/.about__wrapper-->
                    <?php endif;?>

                </div> <!--/.about__text-wrapper-->

                <?php endif; ?>

            </div>
        </div>
    </section> <!--/.about__main-wrapper-->
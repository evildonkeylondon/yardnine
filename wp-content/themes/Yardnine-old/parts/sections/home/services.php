<?php
$title = get_field( 'sv_title' );
$tabs_title = get_field( 'tabs_title' );
?>
    <section id="services-anchor" class="services__main-wrapper">
        
        <div class="services__title section__title">
            
            <?php echo $title;?>
            
        </div>
        
        <div class="services__wrapper"><?php
            /*if($tabs_title):?>
            <h4><?php echo $tabs_title;?></h4><?php
            endif;*/?>
            
            <div class="services__image-wrapper"><?php
                if(have_rows( 'tabs_items' )):
                    
                    $count = 1;
                    while(have_rows( 'tabs_items' )): the_row();
                    $t_img_field = get_sub_field ( 'tab_image' );
                    $t_img = wp_get_attachment_image_src( $t_img_field, 'tabs_img' );?>
                    
                    <div class="tab__img<?php if ($count == 1):?> active<?php endif;?>" id="<?php echo $count;?>" style="background-image:url(<?php echo $t_img[0];?>);">
                    </div><?php
                
                    $count ++;
                    endwhile;
                endif;?>
                
            </div><!--/.services__image-wrapper-->

            <div class="services__tabs"><?php
                if(have_rows( 'tabs_items' )):
                
                    $count = 1;
                
                    while(have_rows( 'tabs_items' )): the_row();
                
                    $t_title = get_sub_field( 'tab_title' );
                    $t_content = get_sub_field( 'tab_content' );
                    
                    ?>
                    
                    <a href="<?php echo $count;?>" class="tab__item <?php if ($count == 1):?>active<?php endif;?>">
                        <span class="plus <?php if ($count == 1):?>open<?php endif;?>"></span>
                        <?php echo $t_title;?>
                        
                    </a>
                    
                    <div class="tab__content <?php if ($count == 1):?>active<?php endif;?>"><?php
                        
                        echo $t_content;?>
                        
                    </div><?php
                $count ++;
                endwhile;
            endif;?>
                
            </div> <!--/.services__tabs-->
            
        </div><!--/.services__wrapper-->
        
    </section> <!--/.services__main-wrapper-->
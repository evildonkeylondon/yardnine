<?php
/**
 * The Header for theme.
 *
 * Displays all of the <head> section and page header
 *
 * @package    WordPress
 * @subpackage themeName
 * @since      themeName 1.0
 */

get_theme_part( 'html-head' );

?>

<body <?php body_class(); ?> data-page="scroll-page">
<div id="page"><?php
    
    $logo_white = get_field( 'logo_v2', 'options' );
    $logo_color = get_field( 'logo', 'options' );
    
    if(is_front_page()):
    
    get_theme_part('sections/home/intro',array(
        'logo_white' =>$logo_white,
        'logo_color' => $logo_color,
        
    ));
    
    get_theme_part('header/header-nav', array(
        'logo' => $logo_color,
        'change' => true
    ));
	
    elseif(is_single() || is_page(7)):
    get_theme_part('header/header-nav-single', array(
        'logo' => $logo_white,
        'logo_color' => $logo_color,
        'class' => 'fixed bg-transparent'
    ));
    
    elseif(is_404()):
    get_theme_part('header/header-nav', array(
        'logo' => $logo_white,
        'logo_color' => $logo_color,
    ));
    
    else:
    get_theme_part('header/header-nav', array(
        'logo' => $logo_color,
        'class' =>'global-header'
    ));
    endif; ?>
    
   
    
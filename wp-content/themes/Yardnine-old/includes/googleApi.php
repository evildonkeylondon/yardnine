<?php 
function my_acf_init() {
	
    $api = get_field('google_api_key', 'options');
	acf_update_setting('google_api_key', $api);
    
}

add_action('acf/init', 'my_acf_init');
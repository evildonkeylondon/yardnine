<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="UTF-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>

	<?php wp_head(); ?>
	<script src="https://maps.googleapis.com/maps/api/js?key=<?php the_field('google_api_key', 'options'); ?>"></script>
</head>

<?php 
$logo = !empty($logo) ? $logo : false;
$logo_color = !empty($logo_color) ? $logo_color : false;
$class = !empty($class) ? $class : false;
$change = !empty($change) ? $change : false;

?>
    <header class="page-main<?php if($class): echo ' ' . $class; endif;?>">
		<div class="container"><?php
            if($logo && $logo_color):?>
                <div class="logo__wrapper<?php if($change):?> change-state<?php endif;?>">
                    <a href="<?php echo esc_url(home_url('/')); ?>"><img src="<?php echo $logo; ?>"></a>
                    <a class="color-logo" href="<?php echo esc_url(home_url('/')); ?>"><img src="<?php echo $logo_color; ?>"></a>
                </div><?php
            elseif($logo && !$logo_color):?>
                <div class="logo__wrapper<?php if($change):?> change-state<?php endif;?>">
                    <a href="<?php echo esc_url(home_url('/')); ?>"><img src="<?php echo $logo; ?>"></a>
                </div><?php
            endif; ?>
            <button class="btn-hamburger"><span></span><span></span><span></span><span></span></button>
			<nav>
				<?php wp_nav_menu( array( 'theme_location' => 'primary', 'container' => false ) ); ?>
			</nav>
		</div>
	</header>
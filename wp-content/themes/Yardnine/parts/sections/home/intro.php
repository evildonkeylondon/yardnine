<?php
$logo_color = !empty($logo_color) ? $logo_color : false;
$logo_white = !empty($logo_white) ? $logo_white : false;

$image = get_field( 'intro_image', 'options' );
$img = wp_get_attachment_image_src($image, 'full');
$intro_text = get_field( 'intro_text', 'options' );

?>

<section class="intro-section">
    <?php
    $rand = array_rand($image, 1);
    if( $image ): ?>
        <div class="intro-section__image" style="background-image:url(<?php echo $image[$rand]['url']; ?>);">
    <?php endif; ?>
       
    </div>
    <div class="intro-section__logo" style="background-image:url(<?php echo $logo_white;?>);"> <!-- $logo_color -->
        <div class="show-image" >
        <img src="<?php echo $logo_white;?>">
        </div>
    </div> 

    <div class="intro-section__text-wrapper">
        <?php echo $intro_text;?>
    </div>
    
    <div class="mask"></div>
</section> <!-- /.intro-section-->
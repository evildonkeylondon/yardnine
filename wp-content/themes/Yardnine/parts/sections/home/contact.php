<?php
$content = get_field( 'ck_content' );
$location = get_field( 'map' );
$icon = get_field( 'social_link_icon' );
$s_link = get_field( 'ct_social_link' );
?>
    <section class="contact__main-wrapper" data-scroll="contact" id="contact">
        <div class="container">
            <div class="contact__text-wrapper"><?php
                if( $content ):
                echo $content;
                endif;?>

                <!-- <a href="<?php echo $s_link['url'];?>" <?php if($s_link['target']):?> target="<?php echo $s_link['target'];?>" <?php endif;?>><img src="<?php echo $icon['url'];?>"></a> -->
            </div><!--/.contact__text-wrapper-->

            <div class="contact__map">
                <div class="acf-map">
                    <div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>"></div>
                </div>
            </div><!--/.cintact__map-->
        </div>
            
    </section> <!--/.contact__main-wrapper-->
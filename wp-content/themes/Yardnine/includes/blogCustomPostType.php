<?php

function news_custom_post_type (){

	$labels = array(
		'name' => 'News',
		'singular_name' => 'News Post',
		'add_new' => 'Add News Post',
		'all_items' => 'All News Posts',
		'add_new_item' => 'Add News Post',
		'edit_item' => 'Edit News Post',
		'new_item' => 'New News Post',
		'view_item' => 'View News Post',
		'search_item' => 'Search News',
		'not_found' => 'No news posts found',
		'not_found_in_trash' => 'No news posts found in trash',
		'parent_item_colon' => 'Parent post'
	);

	$args = array(
		'labels' => $labels,
		'public' => true,
		'order' => 'DESC',
		'has_archive' => true,
		'publicly_queryable' => true,
		'query_var' => true,
		'rewrite' => true,
		'capability_type' => 'post',
		'hierarchical' => false,
		'supports' => array(
			'title',
			'editor',
			'excerpt',
			'thumbnail',
			'revisions',
		),
		'taxonomies' => array('category', 'post_tag'),
		'menu_position' => 5,
		'exclude_from_search' => false
	);
	register_post_type('news', $args);

}
add_action('init','news_custom_post_type');
<?php
/**
 * Theme functions
 * ---------------------------------------------------------------------------------------
 *
 * This file prepares all functionality except theme templates. It automatically
 * includes php files used by theme, therefore no code should be added here.
 * Each piece of functionality should be added as a separate file in the `includes`
 * folder, and it will be automatically included and available in the theme.
 *
 * @package WordPress
 */

/*
 * Initialize the theme core
 */
require_once 'core/init.php';

/*
 * Include the widgets
 * ---------------------------------------------------------------------------------------
 *
 * We only include the files placed directly in the widgets folder. It is the widgets'
 * responsibility to include their own includes and manage their assets.
 *
 */
recursive_include( get_template_directory() . '/widgets', 0 );

//Enqueue Bootstrap Script and Bootstrap CSS
// function gf_resources() {
// 	wp_enqueue_style('css', get_template_directory_uri() . '/css/style.css');
//     wp_enqueue_style('bootstrap', get_template_directory_uri() . '/css/bootstrap.css');
//     wp_enqueue_script( 'bootstrap-js', 'https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.5/js/bootstrap.min.js', array('jquery'), '3.3.4', true );
// }
// add_action('wp_enqueue_scripts', 'gf_resources');
<?php
/**
 * Template Name: Home Page
 * The static page template.
 *
 * @package    WordPress
 * @subpackage YardNine
 * @since      YardNine 1.0
 */

get_header();
the_post();

?>
    
	<main class="content full-width">
        <div class="simple-content__wrapper" data-scroll="home" id="home">
            <div class="container">
                <?php #the_content();?>
                <div class="change-font-size">
                    <?php echo get_first_paragraph(); ?>
                    <a href="#" class="projects__link btn__show-more">More <span class="plus"></span></a>
                    <div class="text__show-more">
                        <?php echo get_the_post(); ?>
                    </div>
                </div>
            </div>
        </div>

        <?php
        get_theme_part('sections/home/partnership');
        get_theme_part('sections/home/about');
        get_theme_part('sections/home/services');
        get_theme_part('sections/home/projects-home');
        get_theme_part('sections/home/contact');
        ?>

	</main>

<?php

get_footer();

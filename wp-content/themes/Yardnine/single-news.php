<?php
/**
 * The single post page template for News page.
 *
 * @package    WordPress
 * @subpackage themeName
 * @since      themeName 1.0
 */

get_header();
the_post();

?>
    
    <main class="content full-width">
        <div class="simple-content__wrapper simple-content__projects-page">

        	<div class="about__content-wrapper">
        		<div class="gf-row">
        			<div class="gf-col-md-12">
        				<h4>News</h4>
        				<h2 style="width: 75%; line-height: 35px;"><?php the_title(); ?></h2>
						<div class="news-post-featuredimage"><?php the_post_thumbnail( 'full' ); ?></div>
					</div>
				</div>
				<div class="gf-row">
					<div class="gf-col-md-9 mobile-padding">
						<p><span class="lightgrey">By</span> <?php the_author(); ?><span class="lightgrey"> on <?php the_time('jS F Y'); ?></span></p>
						<p class="intro-text"><?php the_field('intro_paragraph'); ?></p>
			        	<div class="post-wysiwyg-text"><p><?php the_content();?></p></div>
					</div>
				</div>
				<div class="gf-row">
					<div class="gf-col-md-10">
						<div class="other-image"><img src="<?php echo the_field('image') ?>" /></div>
					</div>
				</div>
				<div class="gf-row">
					<div class="gf-col-md-9 mobile-padding">
						<div class="ending-paragraph"><p><?php the_field('ending_paragraph'); ?></p></div>
					</div>
				</div>
        		
        	</div>

        </div>

    </main>

	<!-- Related News section -->
    <div class="content full-width">
    	<div class="related-news">
       		<div class="container">
       			<h3 style="margin-bottom: 50px;">Related News</h3>
	       			<div class="gf-row">
	       				<?php $the_query = new WP_Query( array( 'post_type' => 'news', 'posts_per_page' => 4 ) ); ?>
	                    <?php if ( $the_query->have_posts() ) :
	                        $counter = 0; ?>
	                        <?php while ( $the_query->have_posts() ) :
	                        $the_query->the_post(); ?>

                                <li class="news-item2 gf-col-md-3">
									<?php $featured_img_url = get_the_post_thumbnail_url($post->ID, 'full'); ?>
                                    <a href="<?php the_permalink(); ?>"><div class="featured-img2" style="background-image: url(<?php echo $featured_img_url; ?>);"></div></a>
                                    <p class="datepublished"><?php the_time('jS F Y'); ?></p>
                                    <a href="<?php the_permalink(); ?>"><h3 class="no-margintop"><?php the_title(); ?></h3></a>
                                </li>
	                            
	                        <?php endwhile; ?>
	                        <?php wp_reset_postdata(); ?>
	                    <?php endif; ?>
	       			</div>
       		</div>
       	</div>
    </div>

<?php

get_footer();

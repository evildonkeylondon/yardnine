<?php
/**
 * Template Name: Projects Page
 * The static page template.
 *
 * @package    WordPress
 * @subpackage YardNine
 * @since      YardNine 1.0
 */

get_header();
the_post();

?>
    
	<main class="content full-width">
        <div class="simple-content__wrapper simple-content__projects-page"><?php
            
            the_content();?>
            
        </div><?php
        
		get_theme_part('sections/home/projects', array(
        'class' => 'projects-page',
        'filter' => true
        ));
        ?>

	</main>

<?php

get_footer();

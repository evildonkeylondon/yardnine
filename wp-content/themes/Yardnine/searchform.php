<?php
/**
 * The search form template.
 *
 * @package    WordPress
 * @subpackage themeName
 * @since      themeName 1.0
 */

?>

<form method="get" action="<?php bloginfo( 'url' ); ?>">
	<input type="text" name="s" id="s"/>
	<input type="submit" value="search"/>
</form>

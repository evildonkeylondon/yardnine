<?php
/**
 * The static page template.
 *
 * @package WordPress
 * @subpackage themeName
 * @since themeName 1.0
 */

get_header();
the_post();

?>

	<main class="content full-width">
	    <div class="container">
		<h1><?php the_title(); ?></h1>

		<?php

		the_content();
		wp_link_pages();

		?>
		</div>
	</main>

<?php

get_footer();

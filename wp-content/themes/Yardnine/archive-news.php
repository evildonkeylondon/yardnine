<?php
/**
 * Template Name: Projects Page
 * The static page template.
 *
 * @package    WordPress
 * @subpackage YardNine
 * @since      YardNine 1.0
 */

get_header();
the_post();

?>
    
	<main class="content full-width">
        <div class="simple-content__wrapper simple-content__projects-page news__wrapper">

            <div class="container">

                <h3 style="margin-left: 3%;"><?php wp_title(''); ?></h3>

                <div class="gf-row">
                    
                    <?php $the_query = new WP_Query( array( 'post_type' => 'news', 'posts_per_page' => 1 ) ); ?>
                    <?php if ( $the_query->have_posts() ) :
                        $counter = 0; ?>
                        <?php while ( $the_query->have_posts() ) :
                        $the_query->the_post(); ?>

                            <ul class="gf-col-md-7">
                                <li class="news-item first-news-item">

                                <?php if (has_post_thumbnail( $post->ID ) ): ?>
                                  <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' ); ?>
                                    <a href="<?php the_permalink(); ?>"><div class="featured-img-main" style="background-image: url(<?php echo $image[0]; ?>); "></div></a>
                                <?php endif; ?>
                                
                                <div class="mobile-padding featured-post">    
                                    <p class="datepublished"><?php the_time('jS F Y'); ?></p>
                                    <a href="<?php the_permalink(); ?>"><h3><?php the_title(); ?></h3></a>
                                    <div class="excerpt"><p><?php the_excerpt(); ?></p></div>
                                </div>
                                
                                </li>

                            </ul>

                        <?php endwhile; ?>
                        <?php wp_reset_postdata(); ?>
                    
                    <?php endif; ?>
                    
                
                    <ul class="second-news-item gf-col-md-5 gf-col-lg-4">
                        <?php $the_query = new WP_Query( array( 'post_type' => 'news', 'posts_per_page' => 1, 'offset' => 1 ) ); ?>
                        <?php if ( $the_query->have_posts() ) :
                            $counter = 0; ?>
                            <?php while ( $the_query->have_posts() ) :
                            $the_query->the_post(); ?>
                                <li class="news-item-3">

                                <?php if (has_post_thumbnail( $post->ID ) ): ?>
                                  <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' ); ?>
                                    <a href="<?php the_permalink(); ?>"><div class="featured-img-2" style="background-image: url(<?php echo $image[0]; ?>); "></div></a>
                                <?php endif; ?>
                                
                                <div class="mobile-padding margin-left20 text-content">
                                    <p class="datepublished"><?php the_time('jS F Y'); ?></p>
                                    <a href="<?php the_permalink(); ?>"><h3><?php the_title(); ?></h3></a>
                                    <div class="excerpt"><p><?php the_excerpt(); ?></p></div>
                                </div>
                                
                                </li>
                            <?php endwhile; ?>
                            <?php wp_reset_postdata(); ?>
                        </ul>
                    <?php endif; ?>
                </div>

            </div>

            <div class="gf-container bottom-news-container">

                <div class="gf-row">
                    <ul class="tablet-margintop10">
                        <?php $the_query = new WP_Query( array( 'post_type' => 'news', 'posts_per_page' => 11, 'offset' => 2 ) ); ?>
                        <?php if ( $the_query->have_posts() ) :
                            $counter = 0; ?>
                            <?php while ( $the_query->have_posts() ) :
                            $the_query->the_post(); ?>

                                
                                <li class="news-item gf-col-md-4">
                                
                                <?php if (has_post_thumbnail( $post->ID ) ): ?>
                                  <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'medium' ); ?>
                                    <a href="<?php the_permalink(); ?>"><div class="featured-img" style="background-image: url(<?php echo $image[0]; ?>); "></div></a>
                                <?php endif; ?>
                                
                                <div class="mobile-padding">    
                                    <p class="datepublished"><?php the_time('jS F Y'); ?></p>
                                    <a href="<?php the_permalink(); ?>"><h3><?php the_title(); ?></h3></a>
                                    <div class="excerpt"><p><?php the_excerpt(); ?></p></div>
                                </div>
                                
                                </li>
                            <?php endwhile; ?>
                            <?php wp_reset_postdata(); ?>
                        </ul>
                    
                    <?php endif; ?>
                    
                </div>

            </div>

            <!-- <div class="gf-container">
                <div class="gf-row">
                    <div class="col-md-12">
                        <?php #echo do_shortcode('[ajax_load_more id="7544327547" container_type="ul" post_type="news" offset="14" posts_per_page="6" pause="true" scroll="false" button_label="Load more +" button_loading_label="Loading..."]'); ?>
                    </div>
                </div>
            </div> -->
            
        </div>

	</main>

<?php

get_footer();
<?php

if ( ! class_exists( 'Cptts_Shortcodes' ) ):

    class Cptts_Shortcodes {

        /**
         * Map
         *
         * @var array
         */
        private $shortcodes = array(
            'custom_button' => 'c_button',
            'clear_func'    => 'clear',
            'tabs_func'     => 'tabs',
            'tab_func'      => 'tab',
            'three_columns' => 'col_3',
            'change_font'   => 'big',
            'text_next_to_img' => 'next'
        );

        function text_next_to_img ($atts, $content) {
           return "<span class='next-text'>" .do_shortcode ( $content ) . "</span>"; 
        }
        
        function change_font ($atts, $content) {
           return "<span class='change-font-size'>" .do_shortcode ( $content ) . "</span>"; 
        }
        
        function three_columns ($atts, $content) {
           return "<div class='col-3-item'>" .do_shortcode ( $content ) . "</div>"; 
        }
        
        function custom_button( $atts ) {
            shortcode_atts( array(
                'href'  => '#',
                'label' => __( 'Anchor', 'cptts' ),
            ), $atts );

            $href  = $atts['href'];
            $label = $atts['label'];

            return "<a href='$href' class='custom-btn'>$label</a>";
        }

        function clear_func() {
            return "<div class='clearfix'></div>";
        }

        function tabs_func( $atts, $content = null ) {
            shortcode_atts( array( 'titles' => '' ), $atts );

            $titles = explode( ",", $atts['titles'] );
            $html   = '<div class="tabs">';

            $html .= '<ul>';
            for ( $i = 1; $i <= count( $titles ); $i++ ) {
                $html .= '<li><a href="#tabs-' . $i . '" rel="tabs-' . $i . '">' . trim( $titles[ $i ] ) . '</a></li>';
            }

            $html .= '</ul>';
            $html .= do_shortcode( $content );
            $html .= '</div>';

            return $html;
        }

        function tab_func( $atts, $content = null ) {
            shortcode_atts( array(
                'id' => ''
            ), $atts );

            $html = '<div id="tabs-' . $atts['id'] . '">';
            $html .= do_shortcode( $content );
            $html .= '</div>';

            return $html;
        }

        /**
         * INTERNAL CLASS FUNCTIONALITY
         */

        /**
         * Cptts_Shortcodes constructor.
         */
        function __construct() {
            add_action( 'init', array( $this, 'create_shorcodes' ) );
        }

        /**
         * Registers all shortcodes defined in $this->shortcodes.
         */
        public function create_shorcodes() {
            foreach ( $this->shortcodes as $func => $name ) {
                add_shortcode( $name, array( $this, $func ) );
            }
        }
    }

    new Cptts_Shortcodes();

endif;
